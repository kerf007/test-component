(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("$"), require("_"));
	else if(typeof define === 'function' && define.amd)
		define("charts", ["$", "_"], factory);
	else if(typeof exports === 'object')
		exports["charts"] = factory(require("$"), require("_"));
	else
		root["charts"] = factory(root["$"], root["_"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__, __WEBPACK_EXTERNAL_MODULE_3__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getElement = (function (fn) {
	var memo = {};

	return function(selector) {
		if (typeof memo[selector] === "undefined") {
			var styleTarget = fn.call(this, selector);
			// Special case to return head of iframe instead of iframe itself
			if (styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[selector] = styleTarget;
		}
		return memo[selector]
	};
})(function (target) {
	return document.querySelector(target)
});

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(8);

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton) options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
	if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertInto + " " + options.insertAt.before);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	options.attrs.type = "text/css";

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	options.attrs.type = "text/css";
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = options.transform(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_3__;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.charts = undefined;

var _chartPlanFact = __webpack_require__(5);

var _chartPlanFact2 = _interopRequireDefault(_chartPlanFact);

var _chartAnalytics = __webpack_require__(10);

var _chartAnalytics2 = _interopRequireDefault(_chartAnalytics);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var charts = exports.charts = { ChartPlanFact: _chartPlanFact2.default, ChartAnalytics: _chartAnalytics2.default };

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

__webpack_require__(6);

var _amchartsStts = __webpack_require__(9);

var _jquery = __webpack_require__(2);

var _jquery2 = _interopRequireDefault(_jquery);

var _lodash = __webpack_require__(3);

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// Подключается на странице
var AmCharts = window.AmCharts;

var POPUP_MARGIN = 20;
var POPUP_MAX_WIDTH = 235;

var ChartPlanFact = function () {
  function ChartPlanFact(domId, data, guideDate) {
    var _this = this;

    var dateFormat = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'DD.MM.YYYY';

    _classCallCheck(this, ChartPlanFact);

    this.popupTimeout = null;

    this.$dom = (0, _jquery2.default)('#' + domId).addClass('chart-plan-fact');

    var stts = _lodash2.default.cloneDeep(_amchartsStts.amchartsStts);
    stts.dataDateFormat = dateFormat;

    stts.guides = [{
      lineAlpha: 0.07,
      tickLength: 0,
      date: guideDate,
      lineThickness: 1
    }];

    // Делаем свои максимумы
    // td height
    // const areaHeight = 230;
    // const popupHeight = 84;

    var processedData = ChartPlanFact.processData(data);
    var max = Math.max(_lodash2.default.maxBy(processedData, 'Plan').Plan, _lodash2.default.maxBy(processedData, 'Fact').Fact);
    var min = Math.min(_lodash2.default.minBy(processedData, 'Plan').Plan, _lodash2.default.minBy(processedData, 'Fact').Fact);

    stts.valueAxes[0].maximum = max * 2.6;
    stts.valueAxes[0].minimum = min;

    stts.dataProvider = processedData;
    this.amChart = AmCharts.makeChart(domId, stts);

    // Убираем напоминание о лицензии
    (0, _jquery2.default)('[title="JavaScript charts"]').remove();

    // Крепим попапы
    this.$popupPlan = (0, _jquery2.default)('<div class="popup"></div>').append('<div><div>План</div><div class="sum"></div></div>').append('<div><div class="plus"></div><div class="minus"></div></div>');

    this.$popupFact = (0, _jquery2.default)('<div class="popup"></div>').append('<div><div>Факт</div><div class="sum"></div></div>').append('<div><div class="plus"></div><div class="minus"></div></div>');

    this.$dom.append(this.$popupPlan, this.$popupFact);

    // Обработчик смены столбца под курсором
    this.amChart.chartCursor.addListener('changed', function (e) {
      _this.curIndex = e.index;

      clearTimeout(_this.popupTimeout);
      _this.$popupPlan.hide();
      _this.$popupFact.hide();

      // Если увели со столбцов - нужно только прятать попапы
      if (e.index === undefined) return;

      // Небольшой таймаут. Это нужно, чтобы svg-dom успел перерисоваться + попапы не обновляется слишком часто
      _this.popupTimeout = setTimeout(function () {
        var svg = e.target.container.container;
        // Находим dom курсора (chartCursor)
        var $g = (0, _jquery2.default)(svg).children('g').eq(2);

        var cursorLeft = $g.offset().left - _this.$dom.offset().left;
        var cursorWidth = +$g.find('rect').attr('width');

        var rightGap = _this.$dom.width() - cursorLeft - cursorWidth;

        if (cursorLeft < POPUP_MAX_WIDTH) {
          _this.$popupPlan.css({ right: 'auto', left: cursorLeft + cursorWidth + POPUP_MARGIN });
          _this.$popupFact.css({ right: 'auto', left: cursorLeft + cursorWidth + POPUP_MARGIN + POPUP_MAX_WIDTH });
        } else if (rightGap < POPUP_MAX_WIDTH) {
          _this.$popupPlan.css({ left: 'auto', right: rightGap + cursorWidth + POPUP_MARGIN + POPUP_MAX_WIDTH });
          _this.$popupFact.css({ left: 'auto', right: rightGap + cursorWidth + POPUP_MARGIN });
        } else {
          _this.$popupPlan.css({ left: 'auto', right: rightGap + cursorWidth + POPUP_MARGIN });
          _this.$popupFact.css({ right: 'auto', left: cursorLeft + cursorWidth + POPUP_MARGIN });
        }

        var item = _this.amChart.dataProvider[e.index];
        var prefix = item.Plan < 0 ? '- ' : '';
        _this.$popupPlan.find('.sum').text(prefix + Math.abs(item.Plan).toLocaleString() + ' ₽');
        _this.$popupPlan.find('.plus').text('+' + item.plan[0].toLocaleString() + ' ₽');
        _this.$popupPlan.find('.minus').text('-' + item.plan[1].toLocaleString() + ' ₽');

        prefix = item.Fact < 0 ? '- ' : '';
        _this.$popupFact.find('.sum').text(item.fact === null ? '--' : prefix + Math.abs(item.Fact).toLocaleString() + ' ₽');
        _this.$popupFact.find('.plus').text(item.fact === null ? '--' : '+ ' + item.fact[0].toLocaleString() + ' ₽');
        _this.$popupFact.find('.minus').text(item.fact === null ? '--' : '- ' + item.fact[1].toLocaleString() + ' ₽');

        _this.$popupPlan.fadeIn();
        _this.$popupFact.fadeIn();
      }, 100);
    });
  }

  _createClass(ChartPlanFact, null, [{
    key: 'processData',
    value: function processData(data) {
      var ret = _lodash2.default.cloneDeep(data);
      var min = Infinity,
          max = -Infinity;
      _lodash2.default.each(ret, function (item) {
        item.Plan = item.plan[0] - item.plan[1];
        item.Fact = item.fact !== null ? item.fact[0] - item.fact[1] : null;
        min = Math.min(min, item.Fact);
        max = Math.max(max, item.Fact);
      });
      _lodash2.default.find(ret, { Fact: min }).minMax = (min / 1000).toLocaleString() + 'K';
      _lodash2.default.find(ret, { Fact: max }).minMax = (max / 1000).toLocaleString() + 'K';
      return ret;
    }
  }]);

  return ChartPlanFact;
}();

exports.default = ChartPlanFact;

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(7);
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {"hmr":true}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__(1)(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../node_modules/css-loader/index.js!./chart-plan-fact.css", function() {
			var newContent = require("!!../../node_modules/css-loader/index.js!./chart-plan-fact.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(undefined);
// imports


// module
exports.push([module.i, ".chart-plan-fact {\r\n    position: relative;\r\n}\r\n\r\n.chart-plan-fact .amcharts-balloon-div {\r\n    top: auto !important;\r\n    bottom: 60px !important;\r\n    /*font-weight: bold;*/\r\n    /*background: #eee;*/\r\n    /*background: red;*/\r\n}\r\n\r\n.chart-plan-fact .popup {\r\n    display: none;\r\n    font-size: 13px;\r\n    position: absolute;\r\n    top: 40px;\r\n    color: #616161;\r\n}\r\n\r\n.chart-plan-fact .popup > div:first-child {\r\n    margin-right: 20px;\r\n    display: inline-block;\r\n    line-height: 21px;\r\n}\r\n.chart-plan-fact .popup > div:last-child {\r\n    display: inline-block;\r\n    line-height: 22px;\r\n    text-align: right;\r\n}\r\n.chart-plan-fact .popup .sum {\r\n    font-size: 19px;\r\n}", ""]);

// exports


/***/ }),
/* 8 */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var BG_COLOR = '#f5f5f5';
var TEXT_COLOR = '#616161';
var TEXT_COLOR2 = '#9e9e9e';

var amchartsStts = exports.amchartsStts = {
  language: 'ru',
  type: 'serial',
  categoryField: 'date',
  sequencedAnimation: false,

  autoMargins: false,
  marginTop: 1,
  marginBottom: 35,
  marginLeft: 35,
  marginRight: 35,

  backgroundAlpha: 1,
  backgroundColor: BG_COLOR,
  fontFamily: 'Arial',
  fontSize: 13,

  categoryAxis: {
    axisThickness: 0,
    gridThickness: 0,
    minHorizontalGap: 25,
    // offset: -25,
    color: TEXT_COLOR,
    parseDates: true,
    markPeriodChange: true,
    minPeriod: 'DD',
    dateFormats: [{ period: 'DD', format: 'DD' }, { period: 'MM', format: 'MMM' }]
  },
  chartCursor: {
    valueBalloonsEnabled: false,
    selectionAlpha: 0.8,
    color: TEXT_COLOR,

    cursorColor: '#eee',
    // cursorColor: '#f0f',
    // categoryBalloonColor: '#f0f',
    categoryBalloonAlpha: 0,
    categoryBalloonColor: 'no', // Единственный способ спрятать не только Balloon, но и его границу

    cursorAlpha: 1,
    fullWidth: true,
    animationDuration: 0
  },
  chartScrollbar: {
    graph: 'Fact',
    scrollbarHeight: 9,
    oppositeAxis: false,
    offset: 60
  },
  graphs: [{
    valueField: 'Fact',
    fillAlphas: 1,
    fillColors: '#77ddec',
    labelText: '[[minMax]]',
    color: TEXT_COLOR2,
    lineThickness: 0,
    negativeFillColors: '#cdcdcd',
    type: 'column',
    showBalloon: false,
    columnWidth: 0.9
  }, {
    valueField: 'Plan',
    fillAlphas: 0,
    dashLength: 7,
    lineColor: '#349FB0',
    negativeLineColor: '#A14646',
    type: 'step'
  }],
  valueAxes: [{
    zeroGridAlpha: 0,
    axisAlpha: 0,
    axisThickness: 0,
    fontSize: 0,
    gridThickness: 0,
    minorGridAlpha: 0,
    minVerticalGap: 30,
    tickLength: 0
  }],
  balloon: {
    animationDuration: 0,
    fadeOutDuration: 0
  }
};

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

__webpack_require__(11);

var _jquery = __webpack_require__(2);

var _jquery2 = _interopRequireDefault(_jquery);

var _lodash = __webpack_require__(3);

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ChartAnalytics = function () {
  function ChartAnalytics(domId, stts, data) {
    var _this = this;

    _classCallCheck(this, ChartAnalytics);

    this.$dom = (0, _jquery2.default)('#' + domId).addClass('chart-analytics');
    this.$table = (0, _jquery2.default)('<table></table>').appendTo(this.$dom);

    _lodash2.default.each(stts, function (item) {
      (0, _jquery2.default)('<tr>').append('<td>' + '<div class="title">' + item.title + '</div>' + '<div class="sum"></div>' + '<div class="descr"></div>' + '</td>').append('<td>' + '<div class="bar" style="width: 0; background-color: ' + item.color + '">' + '<div style="border-color: ' + item.color + '"></div>' + '</div>' + '<div class="percent"></div>' + '</td>').appendTo(_this.$table);
    });
    // setTimeout(() => {
    //   this.updateData(data);
    //
    // }, 1);
    this.updateData(data);
  }

  _createClass(ChartAnalytics, [{
    key: 'updateData',
    value: function updateData(data) {
      this.$table.find('tr').each(function (i, tr) {
        var $tr = (0, _jquery2.default)(tr),
            item = data[i];
        $tr.find('.sum').text(item.sum.toLocaleString() + ' ₽');
        $tr.find('.descr').text(item.descr);
        $tr.find('.percent').text(item.percent + '%');
        setTimeout(function () {
          $tr.find('.bar').css({ width: item.percent + '%' });
        });
      });
    }
  }]);

  return ChartAnalytics;
}();

exports.default = ChartAnalytics;

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(12);
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {"hmr":true}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__(1)(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../node_modules/css-loader/index.js!./chart-analytics.css", function() {
			var newContent = require("!!../../node_modules/css-loader/index.js!./chart-analytics.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(undefined);
// imports


// module
exports.push([module.i, ".chart-analytics {\r\n    background: #f5f5f5;\r\n    padding: 18px;\r\n    color: #616161;\r\n}\r\n.chart-analytics table {\r\n    width: 100%;\r\n}\r\n\r\n.chart-analytics td {\r\n    vertical-align: top;\r\n}\r\n.chart-analytics td:first-child {\r\n    text-align: right;\r\n    width: 200px;\r\n    padding-right: 20px;\r\n    height: 76px;\r\n}\r\n.chart-analytics tr:last-child td:first-child {\r\n    height: auto;\r\n}\r\n.chart-analytics td:last-child {\r\n    padding: 0 20px 0 0;\r\n}\r\n\r\n.chart-analytics .bar {\r\n    height: 20px;\r\n    margin-bottom: 10px;\r\n    position: relative;\r\n    width: 0;\r\n    transition: width 500ms;\r\n}\r\n.chart-analytics .bar > div {\r\n    position: absolute;\r\n    right: -20px;\r\n    border-right: 20px solid transparent !important;\r\n    border-top: 20px solid;\r\n}\r\n\r\n.chart-analytics .descr {\r\n    color: #9e9e9e;\r\n}\r\n\r\n.chart-analytics .percent {\r\n    font-size: 28px;\r\n}\r\n", ""]);

// exports


/***/ })
/******/ ]);
});
//# sourceMappingURL=charts.js.map