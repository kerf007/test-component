const analyticsSettings = [
  {
    title: 'Первичный контракт',
    color: '#b9e986'
  },
  {
    title: 'Готовим предложение',
    color: '#4aabe2'
  },
  {
    title: 'Ожидаем решения',
    color: '#e3d743'
  },
  {
    title: 'В работе',
    color: '#cf9bdb'
  },
  {
    title: 'Подписание акта',
    color: '#e2b268'
  },
  {
    title: 'Успешно реализованно',
    color: '#5dd6a9'
  },
  {
    title: 'Закрыто на реализацию',
    color: '#bdbdbd'
  }
];


const analyticsData0 = [
  {
    percent: 100,
    sum: 150000,
    descr: 'сделок - 60'
  },
  {
    percent: 87,
    sum: 150000,
    descr: 'сделок - 60'
  },
  {
    percent: 77,
    sum: 150000,
    descr: 'сделок - 60'
  },
  {
    percent: 48,
    sum: 150000,
    descr: 'сделок - 60'
  },
  {
    percent: 42,
    sum: 150000,
    descr: 'сделок - 60'
  },
  {
    percent: 67,
    sum: 150000,
    descr: 'сделок - 60'
  },
  {
    percent: 33,
    sum: 150000,
    descr: 'сделок - 60'
  }
];

const analyticsData1 = [
  {
    percent: 33,
    sum: 120000,
    descr: 'сделок - 30'
  },
  {
    percent: 44,
    sum: 120000,
    descr: 'сделок - 60'
  },
  {
    percent: 55,
    sum: 120000,
    descr: 'сделок - 60'
  },
  {
    title: 'В работе',
    color: '#cf9bdb',
    percent: 66,
    sum: 120000,
    descr: 'сделок - 60'
  },
  {
    percent: 77,
    sum: 120000,
    descr: 'сделок - 60'
  },
  {
    percent: 66,
    sum: 120000,
    descr: 'сделок - 60'
  },
  {
    percent: 55,
    sum: 120000,
    descr: 'сделок - 60'
  }
];

