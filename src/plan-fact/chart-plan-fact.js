import './chart-plan-fact.css';
import { amchartsStts } from './amcharts-stts';
import $ from 'jquery';
import _ from 'lodash';

// Подключается на странице
const AmCharts = window.AmCharts;

const POPUP_MARGIN = 20;
const POPUP_MAX_WIDTH = 235;

export default class ChartPlanFact {
  $dom;
  amChart;

  $popupPlan;
  $popupFact;
  popupTimeout = null;

  constructor(domId, data, guideDate, dateFormat = 'DD.MM.YYYY') {
    this.$dom = $('#' + domId).addClass('chart-plan-fact');

    let stts = _.cloneDeep(amchartsStts);
    stts.dataDateFormat = dateFormat;

    stts.guides = [
      {
        lineAlpha: 0.07,
        tickLength: 0,
        date: guideDate,
        lineThickness: 1
      }
    ];

    // Делаем свои максимумы
    // td height
    // const areaHeight = 230;
    // const popupHeight = 84;

    const processedData = ChartPlanFact.processData(data);
    const max = Math.max(_.maxBy(processedData, 'Plan').Plan, _.maxBy(processedData, 'Fact').Fact);
    const min = Math.min(_.minBy(processedData, 'Plan').Plan, _.minBy(processedData, 'Fact').Fact);

    stts.valueAxes[0].maximum = max * 2.6;
    stts.valueAxes[0].minimum = min;

    stts.dataProvider = processedData;
    this.amChart = AmCharts.makeChart(domId, stts);

    // Убираем напоминание о лицензии
    $('[title="JavaScript charts"]').remove();

    // Крепим попапы
    this.$popupPlan = $('<div class="popup"></div>')
      .append('<div><div>План</div><div class="sum"></div></div>')
      .append('<div><div class="plus"></div><div class="minus"></div></div>');

    this.$popupFact = $('<div class="popup"></div>')
      .append('<div><div>Факт</div><div class="sum"></div></div>')
      .append('<div><div class="plus"></div><div class="minus"></div></div>');

    this.$dom.append(this.$popupPlan, this.$popupFact);

    // Обработчик смены столбца под курсором
    this.amChart.chartCursor.addListener('changed', e => {
      this.curIndex = e.index;

      clearTimeout(this.popupTimeout);
      this.$popupPlan.hide();
      this.$popupFact.hide();

      // Если увели со столбцов - нужно только прятать попапы
      if (e.index === undefined) return;

      // Небольшой таймаут. Это нужно, чтобы svg-dom успел перерисоваться + попапы не обновляется слишком часто
      this.popupTimeout = setTimeout(() => {
        const svg = e.target.container.container;
        // Находим dom курсора (chartCursor)
        const $g = $(svg).children('g').eq(2);

        const cursorLeft = $g.offset().left - this.$dom.offset().left;
        const cursorWidth = +$g.find('rect').attr('width');

        const rightGap = this.$dom.width() - cursorLeft - cursorWidth;

        if (cursorLeft < POPUP_MAX_WIDTH) {
          this.$popupPlan.css({right: 'auto', left: cursorLeft + cursorWidth + POPUP_MARGIN});
          this.$popupFact.css({right: 'auto', left: cursorLeft + cursorWidth + POPUP_MARGIN + POPUP_MAX_WIDTH});
        }
        else if (rightGap < POPUP_MAX_WIDTH) {
          this.$popupPlan.css({left: 'auto', right: rightGap + cursorWidth + POPUP_MARGIN + POPUP_MAX_WIDTH});
          this.$popupFact.css({left: 'auto', right: rightGap + cursorWidth + POPUP_MARGIN});
        }
        else {
          this.$popupPlan.css({left: 'auto', right: rightGap + cursorWidth + POPUP_MARGIN});
          this.$popupFact.css({right: 'auto', left: cursorLeft + cursorWidth + POPUP_MARGIN});
        }

        const item = this.amChart.dataProvider[e.index];
        let prefix = item.Plan < 0 ? '- ' : '';
        this.$popupPlan.find('.sum').text(prefix + Math.abs(item.Plan).toLocaleString() + ' ₽');
        this.$popupPlan.find('.plus').text('+' + item.plan[0].toLocaleString() + ' ₽');
        this.$popupPlan.find('.minus').text('-' + item.plan[1].toLocaleString() + ' ₽');

        prefix = item.Fact < 0 ? '- ' : '';
        this.$popupFact.find('.sum').text(item.fact === null ? '--' : prefix + Math.abs(item.Fact).toLocaleString() + ' ₽');
        this.$popupFact.find('.plus').text(item.fact === null ? '--' : '+ ' + item.fact[0].toLocaleString() + ' ₽');
        this.$popupFact.find('.minus').text(item.fact === null ? '--' : '- ' + item.fact[1].toLocaleString() + ' ₽');

        this.$popupPlan.fadeIn();
        this.$popupFact.fadeIn();
      }, 100);

    });

  }

  static processData(data) {
    let ret = _.cloneDeep(data);
    let min = Infinity, max = -Infinity;
    _.each(ret, item => {
      item.Plan = item.plan[0] - item.plan[1];
      item.Fact = item.fact !== null ? item.fact[0] - item.fact[1] : null;
      min = Math.min(min, item.Fact);
      max = Math.max(max, item.Fact);
    });
    _.find(ret, {Fact: min}).minMax = (min / 1000).toLocaleString() + 'K';
    _.find(ret, {Fact: max}).minMax = (max / 1000).toLocaleString() + 'K';
    return ret;
  }
}
