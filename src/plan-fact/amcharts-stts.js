const BG_COLOR = '#f5f5f5';
const TEXT_COLOR = '#616161';
const TEXT_COLOR2 = '#9e9e9e';

export const amchartsStts = {
  language: 'ru',
  type: 'serial',
  categoryField: 'date',
  sequencedAnimation: false,

  autoMargins: false,
  marginTop: 1,
  marginBottom: 35,
  marginLeft: 35,
  marginRight: 35,

  backgroundAlpha: 1,
  backgroundColor: BG_COLOR,
  fontFamily: 'Arial',
  fontSize: 13,

  categoryAxis: {
    axisThickness: 0,
    gridThickness: 0,
    minHorizontalGap: 25,
    // offset: -25,
    color: TEXT_COLOR,
    parseDates: true,
    markPeriodChange: true,
    minPeriod: 'DD',
    dateFormats: [{period: 'DD', format: 'DD'}, {period: 'MM', format: 'MMM'}]
  },
  chartCursor: {
    valueBalloonsEnabled: false,
    selectionAlpha: 0.8,
    color: TEXT_COLOR,

    cursorColor: '#eee',
    // cursorColor: '#f0f',
    // categoryBalloonColor: '#f0f',
    categoryBalloonAlpha: 0,
    categoryBalloonColor: 'no', // Единственный способ спрятать не только Balloon, но и его границу

    cursorAlpha: 1,
    fullWidth: true,
    animationDuration: 0
  },
  chartScrollbar: {
    graph: 'Fact',
    scrollbarHeight: 9,
    oppositeAxis: false,
    offset: 60
  },
  graphs: [
    {
      valueField: 'Fact',
      fillAlphas: 1,
      fillColors: '#77ddec',
      labelText: '[[minMax]]',
      color: TEXT_COLOR2,
      lineThickness: 0,
      negativeFillColors: '#cdcdcd',
      type: 'column',
      showBalloon: false,
      columnWidth: 0.9
    },
    {
      valueField: 'Plan',
      fillAlphas: 0,
      dashLength: 7,
      lineColor: '#349FB0',
      negativeLineColor: '#A14646',
      type: 'step'
    }
  ],
  valueAxes: [
    {
      zeroGridAlpha: 0,
      axisAlpha: 0,
      axisThickness: 0,
      fontSize: 0,
      gridThickness: 0,
      minorGridAlpha: 0,
      minVerticalGap: 30,
      tickLength: 0
    }
  ],
  balloon: {
    animationDuration: 0,
    fadeOutDuration: 0
  }
};
