import ChartPlanFact from 'plan-fact/chart-plan-fact';
import ChartAnalytics from 'analytics/chart-analytics';

export const charts = { ChartPlanFact, ChartAnalytics };
