import './chart-analytics.css';
import $ from 'jquery';
import _ from 'lodash';


export default class ChartAnalytics {
  $dom;
  $table;

  constructor(domId, stts, data) {
    this.$dom = $('#' + domId).addClass('chart-analytics');
    this.$table = $('<table></table>').appendTo(this.$dom);

    _.each(stts, item => {
      $('<tr>')
        .append('<td>' +
            '<div class="title">' + item.title + '</div>' +
            '<div class="sum"></div>' +
            '<div class="descr"></div>' +
          '</td>')
        .append('<td>' +
            '<div class="bar" style="width: 0; background-color: ' + item.color + '">' +
              '<div style="border-color: ' + item.color + '"></div>' +
            '</div>' +
            '<div class="percent"></div>' +
          '</td>')
        .appendTo(this.$table);
    });
    // setTimeout(() => {
    //   this.updateData(data);
    //
    // }, 1);
    this.updateData(data);
  }

  updateData(data) {
    this.$table.find('tr').each((i, tr) => {
      const $tr = $(tr), item = data[i];
      $tr.find('.sum').text(item.sum.toLocaleString() + ' ₽');
      $tr.find('.descr').text(item.descr);
      $tr.find('.percent').text(item.percent + '%');
      setTimeout(() => {
        $tr.find('.bar').css({width: item.percent + '%'});
      });
    });
  }
}